﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CodeSizeCalculator.View
{
    /// <summary>
    /// Логика взаимодействия для CodeMainWindow.xaml
    /// </summary>
    public partial class CodeMainWindow : Window
    {
        public Action OpenCommand;
        public Action EditCommand;

        public CodeMainWindow()
        {
            InitializeComponent();
        }

        private void Open_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            if (OpenCommand != null)
            {
                OpenCommand();
            }
        }

        private void Edit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (EditCommand != null)
            {
                EditCommand();
            }
        }
    }
}
