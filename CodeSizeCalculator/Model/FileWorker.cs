﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace CodeSizeCalculator.Model
{
    public class FileStatistic
    {
        public int Size { get; private set; }
        public int Lines { get; private set; }

        public FileStatistic(int size, int lines)
        {
            Size = size;
            Lines = lines;
        }

        public static FileStatistic operator + (FileStatistic file1, FileStatistic file2)
        {
            if (file1 != null && file2 != null)
            {
                return new FileStatistic(file1.Size + file2.Size, file1.Lines + file2.Lines);
            }
            return null;
        }
    }

    internal static class FileWorker
    {
        internal static Dictionary<string, List<FileInfo>> FindFilesInDictionary(string path)
        {
            DirectoryInfo dictionary = new DirectoryInfo(path);
            string[] extentions = new string[] {".c", ".cpp", ".cs", ".h", ".rc", ".resx" };
            Dictionary<string, List<FileInfo>> result = dictionary.GetFiles("*.*", SearchOption.AllDirectories)
                .Where(f => extentions.Contains(f.Extension))
                .GroupBy(k => k.Extension, f => f)
                .ToDictionary(k => k.Key, f => f.ToList());
            return result;
        }

        internal static FileStatistic CalculateSize(FileInfo file)
        {
            StreamReader reader = new StreamReader(file.FullName);
            int size = 0;
            int count = 0;
            string line = null;
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                if (file.Extension == ".resx")
                {
                    line = Regex.Match(line, @"(?<=<value>)(.*)(?=</value>)").ToString();
                }

                if (!string.IsNullOrWhiteSpace(line))
                {
                    size += line.Length;
                    count++;
                }
            }
            return new FileStatistic(size, count);
        }
    }
}
