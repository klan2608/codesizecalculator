﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CodeSizeCalculator.Model;
using CodeSizeCalculator.ViewModel;
using CodeSizeCalculator.View;

namespace CodeSizeCalculator
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var mainViewModel = new CodeSizeViewModel();
            //{
            //    //Directory = "C:/test/test",
            //    //FileExtentions = new System.Collections.ObjectModel.ObservableCollection<ExtentionInfo>()
            //    //{
            //    //    new ExtentionInfo(true, ".cpp", 2, 123456),
            //    //    new ExtentionInfo(false, ".cs", 3, 26082015)
            //    //}
            //};
            var mainWindow = new CodeMainWindow()
            {
                DataContext = mainViewModel
            };
            mainWindow.OpenCommand = mainViewModel.Open;
            mainWindow.EditCommand = mainViewModel.Edit;
            mainWindow.Show();
        }
    }
}
