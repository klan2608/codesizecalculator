﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace CodeSizeCalculator.ViewModel
{
    public class IconConverter : IValueConverter
    {
        private Icon _icon;

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            _icon = (Icon)value;
            return Imaging.CreateBitmapSourceFromHIcon(_icon.Handle, 
                new System.Windows.Int32Rect(0, 0, _icon.Width, _icon.Height),
                BitmapSizeOptions.FromEmptyOptions());
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return _icon;
        }
    }
}
