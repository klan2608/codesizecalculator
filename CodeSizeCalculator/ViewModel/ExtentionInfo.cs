﻿using CodeSizeCalculator.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace CodeSizeCalculator.ViewModel
{
    public class ExtentionInfo : NotifiHelper
    {
        private bool _enable;
        public bool Enable
        {
            set { _enable = value; NotifyPropertyChanged("Enable"); }
            get { return _enable; }
        }

        private Icon _icon;
        public Icon Icon
        {
            set { _icon = value; NotifyPropertyChanged("Icon"); }
            get { return _icon; }
        }

        private string _extention;
        public string Extention
        {
            set { _extention = value; NotifyPropertyChanged("Extention"); }
            get { return _extention; }
        }

        private int _count;
        public int Count
        {
            set { _count = value; NotifyPropertyChanged("Count"); }
            get { return _count; }
        }

        private int _lines;
        public int Lines
        {
            set { _lines = value; NotifyPropertyChanged("Lines"); }
            get { return _lines; }
        }

        private int _size;
        public int Size
        {
            set { _size = value; NotifyPropertyChanged("Size"); }
            get { return _size; }
        }

        public ExtentionInfo(bool enable, Icon icon, string extention, int count, FileStatistic file)
        {
            Enable = enable;
            Extention = extention;
            Count = count;
            Size = file.Size;
            Lines = file.Lines;
            Icon = icon;
        }
    }
}
