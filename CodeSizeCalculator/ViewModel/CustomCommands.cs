﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CodeSizeCalculator.ViewModel.Commands
{
    public static class CustomCommands
    {
        public static readonly RoutedUICommand Edit = 
            new RoutedUICommand("Edit", "Edit",
                        typeof(CustomCommands),
                        new InputGestureCollection()
                        {
                                new KeyGesture(Key.E, ModifierKeys.Control)
                        });
    }
}
