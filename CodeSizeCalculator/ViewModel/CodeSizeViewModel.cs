﻿using CodeSizeCalculator.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeSizeCalculator.ViewModel
{
    public class CodeSizeViewModel : NotifiHelper
    {
        private string _directory;
        public string Directory
        {
            set { _directory = value; NotifyPropertyChanged("Directory"); }
            get { return _directory; }
        }

        private ObservableCollection<ExtentionInfo> _fileExtentions;
        public ObservableCollection<ExtentionInfo> FileExtentions
        {
            set { _fileExtentions = value; NotifyPropertyChanged("FileExtentions"); }
            get { return _fileExtentions; }
        }

        private int _fullCount;
        public int FullCount
        {
            set { _fullCount = value; NotifyPropertyChanged("FullCount"); }
            get { return _fullCount; }
        }

        private int _fullLines;
        public int FullLines
        {
            set { _fullLines = value; NotifyPropertyChanged("FullLines"); }
            get { return _fullLines; }
        }

        private int _fullSize;
        public int FullSize
        {
            set { _fullSize = value; NotifyPropertyChanged("FullSize"); }
            get { return _fullSize; }
        }

        public CodeSizeViewModel()
        {
            Directory = "";
            FileExtentions = new ObservableCollection<ExtentionInfo>();
        }

        public void Open()
        {
            FolderBrowserDialog dial = new FolderBrowserDialog();
            if (dial.ShowDialog() == DialogResult.OK)
            {
                Directory = dial.SelectedPath;
                FileExtentions = new ObservableCollection<ExtentionInfo>(FileWorker.FindFilesInDictionary(Directory)
                    .Select(e => 
                        new ExtentionInfo(true, Icon.ExtractAssociatedIcon(e.Value.FirstOrDefault().FullName),
                            e.Key, 
                            e.Value.Count, 
                            e.Value.Aggregate(new FileStatistic(0,0), (acc, a) => acc += FileWorker.CalculateSize(a)))));
                Edit();
            }
        }

        public void Edit()
        {
            FullSize = FileExtentions.Aggregate(0, (acc, a) => acc += a.Enable ? a.Size : 0);
            FullLines = FileExtentions.Aggregate(0, (acc, a) => acc += a.Enable ? a.Lines : 0);
            FullCount = FileExtentions.Aggregate(0, (acc, a) => acc += a.Enable ? a.Count : 0);
        }
    }
}
