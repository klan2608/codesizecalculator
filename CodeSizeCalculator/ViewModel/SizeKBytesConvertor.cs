﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CodeSizeCalculator.ViewModel
{
    public class SizeKBytesConvertor : IValueConverter
    {
        private int _size;

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            _size = (int)value;
            double newSize = _size / 1024.0;
            return string.Format("{0:F3} k", newSize);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return _size;
        }
    }
}
